import React from 'react';

import Layout from './components/Layout.js';

const App: React.FC = () => {
  return (
    <div className="App">
      <Layout/>
    </div>
  );
}

export default App;