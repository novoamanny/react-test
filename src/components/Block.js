import React from 'react';
import DeleteBTN from './DeleteBTN';


const Block = ({id ,title, text, onDelete}) =>{
    return(
        <div id={id} style={{padding: '10px 40px', margin: 20, borderRadius: 20, background: '#2f2f2f'}}>
            
            <DeleteBTN id={id} onDelete={onDelete}/>
            
            <div>
            
            <h4 style={{color: '#fff', fontSize: 12, textAlign: 'center'}}>{title}</h4>
            
            </div>
            
            <div style={{}}>
            <h3 style={{color: '#fff', fontSize: 20, textAlign: 'center'}}>{text}</h3>
            </div>
        </div>
    )
}

export default Block