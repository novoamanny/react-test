import React from 'react';
import Block from './Block.js'

const BlockContainer = ({blocks, onDelete}) =>{
    return(
        <div style={{display: 'flex', flexWrap: 'wrap', margin: 20 , width: '100%'}}>
            {blocks && blocks.map(block =>{
                return <Block id={block.id} title={block.title} text={block.text} onDelete={onDelete}/>
            })}
        </div>
    )
}

export default BlockContainer;