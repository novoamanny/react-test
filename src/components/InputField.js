import React, {useState} from 'react';
import SubmitBTN from './SubmitBTN.js';


const InputField = ({onTextChange, text, onSubmitHandle}) =>{

    

    return(
        <div style={{margin: 20, display: 'flex', justifyContent: 'center'}}>
            <input style={{background: '#2f2f2f', color: '#fff', padding: '5px 10px', borderRadius: 10}} type='text' value={text} onChange={ e => onTextChange(e)}/>
            <SubmitBTN onSubmitHandle={onSubmitHandle}/>
        </div>
    )
}

export default InputField;