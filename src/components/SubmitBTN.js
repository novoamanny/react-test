import React from 'react';

const SubmitBTN = ({onSubmitHandle}) =>{
    return(
        <input type='submit' onClick={(e) => onSubmitHandle(e)}/>
    )
}

export default SubmitBTN;