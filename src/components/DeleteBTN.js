import React from 'react';
import { relative } from 'path';

import './styles/DeleteBTN.css'

const DeleteBTN = ({id, onDelete}) =>{
    return(
        <div className='deleteBTN' onClick={() => onDelete(id)}>
            <i style={{color: '#fff', fontSize: 20}}>x</i>
        </div>
    )
}

export default DeleteBTN;
