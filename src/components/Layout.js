import React, {useState} from 'react';

import InputField from './InputField';

import BlockContainer from './BlockContainer.js';

const Layout = () =>{

    const [text, setText] = useState('')

    const [blocks, setBlocks] = useState([]);

    const onTextChange = (e) =>{
        setText(e.target.value);
    }

    const onSubmitHandle = (e) =>{
        e.preventDefault();

        let list = text.split('|');
        let tempList
        tempList = list.filter(item =>{
            if(item === ""){
                return null;
            }else{
                return item;
            }
        })

        tempList.forEach(item =>{
           
            if(item.indexOf(':')){
                // let TList = item.split(':');
                let newList = item.split(':');

                
                setBlocks(prevItems =>{
                    return [{id: prevItems.length, title: newList[0], text: newList[1]},...prevItems]
                });
                
            }
            
        })

    }

    const onDelete=(id) =>{
        setBlocks(prevItems => {
            return prevItems.filter(item => item.id != id);
        })
    }

    

    return(
        <div >
        
            <InputField onTextChange={onTextChange} text={text} onSubmitHandle={onSubmitHandle}/>
            <BlockContainer blocks={blocks} onDelete={onDelete}/>
        </div>
    )
}

export default Layout;